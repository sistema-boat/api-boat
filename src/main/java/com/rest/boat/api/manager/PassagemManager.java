package com.rest.boat.api.manager;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.rest.boat.api.domain.dto.PassagemDTO;
import com.rest.boat.api.domain.dto.PassagemNewDTO;
import com.rest.boat.api.domain.enums.StatusPassagem;
import com.rest.boat.api.domain.model.Cliente;
import com.rest.boat.api.domain.model.Passagem;
import com.rest.boat.api.domain.model.Viagem;
import com.rest.boat.api.manager.exception.DataIntegrityException;
import com.rest.boat.api.manager.exception.OjectNotFoundException;
import com.rest.boat.api.repositories.PassagemRepository;

@Component
public class PassagemManager {

	@Autowired
	private PassagemRepository passagemRepository;

	@Autowired
	private ClienteManager clienteManager;
	@Autowired
	private ViagemManager viagemManager;

	public List<PassagemDTO> listarPassagens() {
		List<Passagem> passagens = passagemRepository.findAll();
		List<PassagemDTO> list = new ArrayList();
		
		for (Passagem p : passagens) {
			list.add(new PassagemDTO(p));
		}
		return list;
	}
	
	
	
	public Passagem find(Long id) {
		Optional<Passagem> passagem = passagemRepository.findById(id);
		return passagem.orElseThrow(
				() -> new OjectNotFoundException("passagem não encontrada! Id: " + id + ",Tipo: " + Passagem.class));
	}
	

//	public Optional<Passagem> find(Long id) {
//		Optional<Passagem> passagem = passagemRepository.findById(id);
//		if (passagem.isPresent()) {
//			return passagem;
//		}
//		throw new OjectNotFoundException("Cliente não encontrado");
//	}

	
	
	public Passagem insert(Passagem obj) {
		obj.setId(null);		
		return passagemRepository.save(obj);
	}

	public Passagem fromDTO(PassagemNewDTO objNewDto) {
		Viagem viagem = viagemManager.find(objNewDto.getViagem().getId());
		Cliente cliente = clienteManager.find(objNewDto.getCliente().getId());
		Passagem passagem = new Passagem(null, objNewDto.getPoltrona(), objNewDto.getDataDaCompra(), cliente,
				viagem, objNewDto.getTipoVenda(),objNewDto.getStatusPassagem());
		return passagem;
	}

	@Transactional
	public PassagemNewDTO update(PassagemNewDTO passagemDto) {
		Optional<Passagem> optional = passagemRepository.findById(passagemDto.getId());
		if(optional.isPresent()) {
			Passagem passagem = optional.get();
			passagem.setPoltrona(passagemDto.getPoltrona());
			passagem.setViagem(passagemDto.getViagem());
			passagem.setStatusPassagem(StatusPassagem.find(passagemDto.getStatusPassagem()));
			return new PassagemNewDTO();
		} else {
			throw new OjectNotFoundException("Passagem não existe");
		}		
	}
	
	public void delete (Long id) {
		find(id);
		try {
			passagemRepository.deleteById(id);		
		} catch (DataIntegrityViolationException e) {
			throw new DataIntegrityException("Não é possivel deleta passagem que possui viagem");
		}

	}
	
}
