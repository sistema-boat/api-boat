package com.rest.boat.api.manager;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.rest.boat.api.domain.dto.ClienteDTO;
import com.rest.boat.api.domain.dto.ClienteNewDTO;
import com.rest.boat.api.domain.enums.FaixaEtariaEnum;
import com.rest.boat.api.domain.enums.SexoEnum;
import com.rest.boat.api.domain.enums.TipoDocumentoEnum;
import com.rest.boat.api.domain.enums.TipoPessoaEnum;
import com.rest.boat.api.domain.model.Cliente;
import com.rest.boat.api.domain.model.EnderecoCliente;
import com.rest.boat.api.domain.model.TelefoneCliente;
import com.rest.boat.api.manager.exception.DataIntegrityException;
import com.rest.boat.api.manager.exception.OjectNotFoundException;
import com.rest.boat.api.repositories.ClienteRepository;
import com.rest.boat.api.repositories.EnderecoRepository;
import com.rest.boat.api.repositories.TelefoneClienteRepository;

@Component
public class ClienteManager {
	@Autowired
	private ClienteRepository clienteRepository;
	@Autowired
	private EnderecoRepository enderecoRepository;

	@Autowired
	private TelefoneClienteRepository telefoneClienteRepository;

	public Cliente find(Long id) {
		Optional<Cliente> cliente = clienteRepository.findById(id);
		return cliente.orElseThrow(
				() -> new OjectNotFoundException("Cliente não encontrado! Id: " + id + ",Tipo: " + Cliente.class));
	}


	public List<ClienteDTO> listarCleinte() {
		List<Cliente> clientes = clienteRepository.findAlld1();
		List<ClienteDTO> list = new ArrayList<>();
		for (Cliente c : clientes) {
			list.add(new ClienteDTO(c));
		}
		return list;
	}

	@Transactional
	public Cliente insert(Cliente obj) {
		obj.setId(null);
		obj = clienteRepository.save(obj);
		enderecoRepository.saveAll(obj.getEnderecos());
		telefoneClienteRepository.saveAll(obj.getTelefones());
		return obj;
	}

	@Transactional
	public ClienteNewDTO update(ClienteNewDTO clienteDto) {
		Optional<Cliente> optional = clienteRepository.findById(clienteDto.getId());
		if (optional.isPresent()) {
			EnderecoCliente enderecoCliente = enderecoRepository.findByClienteId(clienteDto.getId());
			TelefoneCliente telefoneCliente = telefoneClienteRepository.findByClienteId(clienteDto.getId());
			Cliente cliente = optional.get();
			cliente.setNome(clienteDto.getNome());
			cliente.setNomeMae(clienteDto.getNomeMae());
			cliente.setDataNascimento(clienteDto.getDataNascimento());
			cliente.setSexo(SexoEnum.find(clienteDto.getSexo()));
			cliente.setRg(clienteDto.getRg());
			cliente.setTipoPessoa(TipoPessoaEnum.find(clienteDto.getTipoPessoa()));
			cliente.setTipoDocumento(TipoDocumentoEnum.find(clienteDto.getTipoDocumento()));
			cliente.setCpfOuCnpj(clienteDto.getCpfOuCnpj());
			cliente.setFaixaEtaria(FaixaEtariaEnum.find(clienteDto.getFaixaEtaria()));
			cliente.setEmail(clienteDto.getEmail());
			enderecoCliente.setCep(clienteDto.getCep());
			enderecoCliente.setLogradouro(clienteDto.getLogradouro());
			enderecoCliente.setComplemento(clienteDto.getComplemento());
			enderecoCliente.setBairro(clienteDto.getBairro());
			enderecoCliente.setLocalidade(clienteDto.getLocalidade());
			enderecoCliente.setUf(clienteDto.getUf());
			enderecoCliente.setDdd(clienteDto.getDdd());
			telefoneCliente.setTelefone1(clienteDto.getTelefone1());
			telefoneCliente.setTelefone2(clienteDto.getTelefone2());
			telefoneCliente.setTelefone3(clienteDto.getTelefone3());
			clienteRepository.save(cliente);
			enderecoRepository.save(enderecoCliente);
			telefoneClienteRepository.save(telefoneCliente);
			return new ClienteNewDTO();
		} else {
			throw new OjectNotFoundException("Cliente não existe");
		}

	}

	public void delete(Long id) {
		find(id);
		try {

			clienteRepository.deleteById(id);
			throw new OjectNotFoundException("Cliente deletado com sucesso");
		} catch (DataIntegrityViolationException e) {
			throw new DataIntegrityException("Não é possivel deleta embarcacão que possui funcionario");
		}
	}

	public Page<Cliente> findPage(Integer page, Integer linesPerPage, String orderBy, String direction) {
		PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);
		return clienteRepository.findAll(pageRequest);
	}

	public Cliente fromDTO(ClienteNewDTO clienteNewDTO) {
		Cliente cli = new Cliente(null, clienteNewDTO.getNome(), clienteNewDTO.getNomeMae(),
				clienteNewDTO.getDataNascimento(), clienteNewDTO.getSexo(), clienteNewDTO.getRg(),
				clienteNewDTO.getTipoPessoa(), clienteNewDTO.getTipoDocumento(), clienteNewDTO.getCpfOuCnpj(),
				clienteNewDTO.getFaixaEtaria(), clienteNewDTO.getEmail());
		EnderecoCliente end = new EnderecoCliente(null, clienteNewDTO.getCep(), clienteNewDTO.getLogradouro(),
				clienteNewDTO.getComplemento(), clienteNewDTO.getBairro(), clienteNewDTO.getLocalidade(),
				clienteNewDTO.getUf(), clienteNewDTO.getDdd(), cli);

		TelefoneCliente telefoneCliente = new TelefoneCliente(null, clienteNewDTO.getTelefone1(),
				clienteNewDTO.getTelefone2(), clienteNewDTO.getTelefone3(), cli);

		cli.getEnderecos().add(end);
		cli.getTelefones().add(telefoneCliente);
		return cli;

	}
}
