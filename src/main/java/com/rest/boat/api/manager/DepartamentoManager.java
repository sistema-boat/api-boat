package com.rest.boat.api.manager;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.rest.boat.api.domain.dto.DepartamentoNewDTO;
import com.rest.boat.api.domain.model.Departamento;
import com.rest.boat.api.repositories.DepartamentoRepository;

@Component
public class DepartamentoManager {

	@Autowired
	private DepartamentoRepository departamentoRepository;

	@Transactional
	public Departamento inserir(Departamento obj) {
		obj.setId(null);
		obj = departamentoRepository.save(obj);
		return obj;
	}
	
	public Departamento fromDTO(DepartamentoNewDTO departamentoNewDTO) {
		Departamento dp = new Departamento(null, departamentoNewDTO.getNome());
		return dp;
	}
}
