package com.rest.boat.api.manager;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.rest.boat.api.domain.dto.EmpresaDTO;
import com.rest.boat.api.domain.dto.EmpresaNewDTO;
import com.rest.boat.api.domain.model.Empresa;
import com.rest.boat.api.domain.model.EnderecoEmpresa;
import com.rest.boat.api.domain.model.TelefoneEmpresa;
import com.rest.boat.api.manager.exception.OjectNotFoundException;
import com.rest.boat.api.repositories.EmpresaRepository;
import com.rest.boat.api.repositories.EnderecoEmpresaRepository;
import com.rest.boat.api.repositories.TelefoneEmpresaRepository;

@Component
public class EmpresaManager {
	@Autowired
	private EmpresaRepository empresaRepository;
	@Autowired
	private EnderecoEmpresaRepository enderecoEmpresaRepository;
	@Autowired
	private TelefoneEmpresaRepository telefoneEmpresaRepository;
	
	
	public Empresa find(Long id) {
		Optional<Empresa> empresa = empresaRepository.findById(id);
		return empresa.orElseThrow(
				() -> new OjectNotFoundException("Empresa não encontrado! Id: " + id + ",Tipo: " + Empresa.class));
	}
	
	public Empresa insert(Empresa obj) {
		obj.setId(null);
		obj = empresaRepository.save(obj);
		enderecoEmpresaRepository.saveAll(obj.getEnderecos());
		telefoneEmpresaRepository.saveAll(obj.getTelefones());
		return  obj;
	}
	
	public Empresa fromDTO(EmpresaNewDTO objtDto) {
		Empresa empresa = new Empresa(null,objtDto.getNome().toUpperCase(), objtDto.getNomeFantasia().toUpperCase(), objtDto.getCnpj());
		EnderecoEmpresa endereco = new EnderecoEmpresa(null, objtDto.getCep(), objtDto.getLogradouro(), objtDto.getComplemento(), objtDto.getBairro(), objtDto.getLocalidade(), objtDto.getUf(), objtDto.getDdd(), empresa);
		TelefoneEmpresa telefone = new TelefoneEmpresa(null, objtDto.getTelefone1(), objtDto.getTelefone2(), objtDto.getTelefone3(), empresa);
		empresa.getEnderecos().add(endereco);
		empresa.getTelefones().add(telefone);
		return empresa;
	}

	@Transactional
	public EmpresaNewDTO update(EmpresaNewDTO empresaDto) {
		Optional<Empresa> optional = empresaRepository.findById(empresaDto.getId());
		if(optional.isPresent()) {
		EnderecoEmpresa enderecoEmpresa = enderecoEmpresaRepository.findByEmpresaId(empresaDto.getId());
		TelefoneEmpresa telefoneEmpresa = telefoneEmpresaRepository.findByEmpresaId(empresaDto.getId());
		Empresa empresa = optional.get();
		empresa.setNome(empresaDto.getNome().toUpperCase());
		empresa.setNomeFantasia(empresaDto.getNomeFantasia().toUpperCase());
		enderecoEmpresa.setCep(empresaDto.getCep());
		enderecoEmpresa.setLogradouro(empresaDto.getLogradouro());
		enderecoEmpresa.setComplemento(empresaDto.getComplemento());
		enderecoEmpresa.setBairro(empresaDto.getBairro());
		enderecoEmpresa.setLocalidade(empresaDto.getLocalidade());
		enderecoEmpresa.setUf(empresaDto.getUf());
		telefoneEmpresa.setTelefone1(empresaDto.getTelefone1());
		telefoneEmpresa.setTelefone2(empresaDto.getTelefone2());
		telefoneEmpresa.setTelefone3(empresaDto.getTelefone3());
			return new EmpresaNewDTO();
		} else {
			return null;
		}
		
	}

	public void delete(Long id) {
		find(id);
		try {
			empresaRepository.deleteById(id);
			
		} catch (DataIntegrityViolationException e) {
		throw new DataIntegrityViolationException("Não foi possivel deleta a empresa pois possui embarcação");
		}
		
	}

	public List<EmpresaDTO> listarCleinte() {
		List<Empresa> empresas = empresaRepository.findAll();
		List<EmpresaDTO> list = new ArrayList<>();
		for (Empresa c : empresas) {
			list.add(new EmpresaDTO(c));
		}
		return list;
	}

	
}
