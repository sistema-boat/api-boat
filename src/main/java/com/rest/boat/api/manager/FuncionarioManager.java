package com.rest.boat.api.manager;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.rest.boat.api.domain.dto.FuncionarioDTO;
import com.rest.boat.api.domain.dto.FuncionarioNewDTO;
import com.rest.boat.api.domain.enums.EstadoCivilEnum;
import com.rest.boat.api.domain.enums.SexoEnum;
import com.rest.boat.api.domain.model.EnderecoFuncionario;
import com.rest.boat.api.domain.model.Funcionario;
import com.rest.boat.api.domain.model.TelefoneFuncionario;
import com.rest.boat.api.manager.exception.DataIntegrityException;
import com.rest.boat.api.manager.exception.OjectNotFoundException;
import com.rest.boat.api.repositories.EnderecoFuncionarioRepository;
import com.rest.boat.api.repositories.FuncionarioRepository;
import com.rest.boat.api.repositories.TelefoneFuncionarioRepository;

@Component
public class FuncionarioManager {

	@Autowired
	private FuncionarioRepository funcionarioRepository;

	@Autowired
	private TelefoneFuncionarioRepository telefoneFuncionarioRepository;
	@Autowired
	private EnderecoFuncionarioRepository enderecoFuncionarioRepository;

	public Funcionario find(Long id) {
		Optional<Funcionario> funcionario = funcionarioRepository.findById(id);
		return funcionario.orElseThrow(() -> new OjectNotFoundException(
				"Funcionario não encontrado! Id: " + id + ",Tipo: " + Funcionario.class));
	}

	@Transactional
	public Funcionario insert(Funcionario obj) {
		obj.setId(null);
		obj = funcionarioRepository.save(obj);
		enderecoFuncionarioRepository.saveAll(obj.getEnderecos());
		telefoneFuncionarioRepository.saveAll(obj.getTelefones());
		return obj;
	}

	public Funcionario fromDTO(FuncionarioNewDTO funcionarioNewDTO) {
		Funcionario funcionario = new Funcionario(null, funcionarioNewDTO.getNome(), funcionarioNewDTO.getNomeDaMae(),
				funcionarioNewDTO.getDataNascimento(), funcionarioNewDTO.getSexo(), funcionarioNewDTO.getCpf(),
				funcionarioNewDTO.getRg(), funcionarioNewDTO.getEmail(), funcionarioNewDTO.getEstadoCivil(), null);
		EnderecoFuncionario enderecoFuncionario = new EnderecoFuncionario(null, funcionarioNewDTO.getCep(),
				funcionarioNewDTO.getLogradouro(), funcionarioNewDTO.getComplemento(), funcionarioNewDTO.getBairro(),
				funcionarioNewDTO.getLocalidade(), funcionarioNewDTO.getUf().toUpperCase(), funcionarioNewDTO.getDdd(), funcionario);
		TelefoneFuncionario telefoneFuncionario = new TelefoneFuncionario(null, funcionarioNewDTO.getTelefone1(),
				funcionarioNewDTO.getTelefone2(), funcionarioNewDTO.getTelefone3(), funcionario);

		funcionario.getEnderecos().add(enderecoFuncionario);
		funcionario.getTelefones().add(telefoneFuncionario);
		return funcionario;

	}

	@Transactional
	public void delete(Long id) {
		find(id);
		try {
			funcionarioRepository.deleteById(id);
			System.out.println("Funcionario deletado com sucesso");
		} catch (DataIntegrityViolationException e) {
			throw new DataIntegrityException("Não é possivel deleta funcionario que possui embarcação");
		}

	}

	public List<FuncionarioDTO> listarFuncionario() {
		List<Funcionario> funcionarios = funcionarioRepository.findAll();
		List<FuncionarioDTO> list = new ArrayList<>();
		for (Funcionario f : funcionarios) {
			list.add(new FuncionarioDTO(f));
		}

		return list;
	}

	@Transactional
	public FuncionarioNewDTO update(FuncionarioNewDTO funcionarioDto) {
		Optional<Funcionario> funcionarios = funcionarioRepository.findById(funcionarioDto.getId());
		if (funcionarios.isPresent()) {
			Funcionario funcionario = funcionarios.get();
			funcionario.setNome(funcionarioDto.getNome());
			funcionario.setNomeDaMae(funcionarioDto.getNomeDaMae());
			funcionario.setDataNascimento(funcionarioDto.getDataNascimento());
			funcionario.setEmail(funcionarioDto.getEmail());
			funcionario.setSexo(SexoEnum.find(funcionarioDto.getSexo()));
			funcionario.setEstadoCivil(EstadoCivilEnum.find(funcionarioDto.getEstadoCivil()));			
			EnderecoFuncionario enderecoFuncionario = enderecoFuncionarioRepository.findByFuncionarioId(funcionarioDto.getId());
			enderecoFuncionario.setCep(funcionarioDto.getCep());
			enderecoFuncionario.setLogradouro(funcionarioDto.getLogradouro());
			enderecoFuncionario.setLocalidade(funcionarioDto.getLocalidade());
			enderecoFuncionario.setComplemento(funcionarioDto.getComplemento());
			enderecoFuncionario.setBairro(funcionarioDto.getBairro());
			enderecoFuncionario.setUf(funcionarioDto.getUf().toUpperCase());
			return new FuncionarioNewDTO();
		} else {
			return null;
		}
	}

}
