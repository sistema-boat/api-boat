package com.rest.boat.api.manager;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;


import com.rest.boat.api.domain.dto.EmbarcacaoNewDTO;
import com.rest.boat.api.domain.model.Embarcacao;
import com.rest.boat.api.domain.model.Empresa;
import com.rest.boat.api.manager.exception.DataIntegrityException;
import com.rest.boat.api.manager.exception.OjectNotFoundException;
import com.rest.boat.api.repositories.EmbarcacaoRepository;

@Component
public class EmbarcacaoManager {

	@Autowired
	private EmbarcacaoRepository embarcacaoRepository;

	@Autowired
	private EmpresaManager empresaManager;
	

	public Embarcacao find(Long id) {
		Optional<Embarcacao> embarcacao = embarcacaoRepository.findById(id);
		return embarcacao.orElseThrow(
				() -> new OjectNotFoundException("Embarcacao não encontrada! Id: " + id + ",Tipo:" + Embarcacao.class));
	}

	public Embarcacao insert(Embarcacao obj) {
		obj.setId(null);
		obj = embarcacaoRepository.save(obj);
		return obj;
	}

	public void delete(Long id) {
		find(id);
		try {
			embarcacaoRepository.deleteById(id);
			throw new OjectNotFoundException("Cliente deletado com sucesso! id: " + id + ",Tipo" + Embarcacao.class);
		} catch (DataIntegrityViolationException e) {
			throw new DataIntegrityException("Não é possivel deleta embarcacão que possui funcionario");
		}

	}

	public List<Embarcacao> findAll() {
		return embarcacaoRepository.findAll();
	}

	
	
	public Page<Embarcacao> findPage(Integer page, Integer linesPerPage, String orderBy, String direction) {
		PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);
		return embarcacaoRepository.findAll(pageRequest);
	}

	public Embarcacao fromDTO(EmbarcacaoNewDTO objDto) {
		Empresa empresa = empresaManager.find(objDto.getEmpresa().getId());
		Embarcacao embarcacao = new Embarcacao(null, objDto.getNome(), objDto.getCapacidade(), objDto.getNumIdenMar(), empresa);
		return embarcacao;
	}

	public EmbarcacaoNewDTO update(EmbarcacaoNewDTO embarcacaoNewDto) {
		Optional<Embarcacao>  optional = embarcacaoRepository.findById(embarcacaoNewDto.getId());
		if (optional.isPresent()) {
			Embarcacao embarcacao = optional.get();
			embarcacao.setEmpresa(embarcacaoNewDto.getEmpresa());
			embarcacao.setNome(embarcacaoNewDto.getNome());
			embarcacao.setCapacidade(embarcacaoNewDto.getCapacidade());
			embarcacao.setNumIdenMar(embarcacaoNewDto.getNumIdenMar());
			embarcacaoRepository.save(embarcacao);
			return new EmbarcacaoNewDTO();
		} else {
		
			throw new OjectNotFoundException("Embarcação não existe");
		}
		
	}
}
