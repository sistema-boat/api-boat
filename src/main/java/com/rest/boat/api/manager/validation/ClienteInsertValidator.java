package com.rest.boat.api.manager.validation;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.rest.boat.api.controller.exception.FieldMessage;
import com.rest.boat.api.domain.dto.ClienteNewDTO;
import com.rest.boat.api.domain.enums.TipoPessoaEnum;
import com.rest.boat.api.manager.validation.utils.BR;

public class ClienteInsertValidator implements ConstraintValidator<ClienteInsert, ClienteNewDTO> {

//	@Autowired
//	private ClienteRepository repo;

	@Override
	public void initialize(ClienteInsert ann) {
		// Coloca uma programação de inicialização
	}

	@Override
	public boolean isValid(ClienteNewDTO objDto, ConstraintValidatorContext context) {
		List<FieldMessage> list = new ArrayList<>();

		if (objDto.getTipoPessoa().equalsIgnoreCase(TipoPessoaEnum.PESSOAFISICA.toString())
				&& !BR.isValidCPF(objDto.getCpfOuCnpj())) {
			list.add(new FieldMessage("cpfOuCnpj", "CPF inválido"));
		}

		if (objDto.getTipoPessoa().equalsIgnoreCase(TipoPessoaEnum.PESSOAJURIDICA.toString())
				&& !BR.isValidCNPJ(objDto.getCpfOuCnpj())) {
			list.add(new FieldMessage("cpfOuCnpj", "CNPJ Inválido"));
		}

		for (FieldMessage e : list) {
			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate(e.getMessage()).addPropertyNode(e.getFieldName())
					.addConstraintViolation();
		}
		return list.isEmpty();
	}

}