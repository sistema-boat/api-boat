package com.rest.boat.api.manager;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.rest.boat.api.domain.dto.ViagemDTO;
import com.rest.boat.api.domain.dto.ViagemNewDTO;
import com.rest.boat.api.domain.model.Viagem;
import com.rest.boat.api.manager.exception.OjectNotFoundException;
import com.rest.boat.api.repositories.ViagemRepository;

@Component
public class ViagemManager {
	

	@Autowired
	private ViagemRepository viagemRepository;

	@Autowired
	private EmbarcacaoManager embarcacaoManager;


	public Viagem find(Long id) {
		Optional<Viagem> viagem = viagemRepository.findById(id);
		return viagem.orElseThrow(
				() -> new OjectNotFoundException("Viagem não encontrada! Id:" + id + ", Tipo: " + Viagem.class));
	}

//	@Transactional
//	public Viagem insert(Viagem obj) {
//		obj.setCliente(clientemanager.find(obj.getCliente().getId()));
//		return viagemRepository.save(obj);
//	}
//	

	public List<ViagemDTO> listarViagem() {
		List<Viagem> viagens = viagemRepository.findAll();
		List<ViagemDTO> list = new ArrayList<>();
		for (Viagem v : viagens) {
			list.add(new ViagemDTO(v));
		}
		return list;
	}
	
	
	@Transactional
	public Viagem insert(Viagem obj) {
		return viagemRepository.save(obj);
	}

	public Viagem fromDTO(ViagemNewDTO objDto) {
		//Embarcacao embarcacao = embarcacaoManager.find(objDto.getEmbarcacao().getId());
		Viagem viagem = new Viagem(null, objDto.getDestino(), objDto.getOrigem(), objDto.getDataSaida(),
				objDto.getDataChegada(), null);
		return viagem;
	}

	public List<ViagemDTO> getViagemByOrigem(String origem, String destino) {
		List<Viagem> viagens = viagemRepository.findByOrigemAndDestino(origem, destino);
		List<ViagemDTO> list = new ArrayList<>();
		for (Viagem viagem : viagens) {
			list.add(new ViagemDTO(viagem));
		}
		return list;
	}

//	public List<ViagemDTO> getViagemByDataSaida(Date dataSaida, Date datachegada) {
//		List<Viagem> viagens = viagemRepository.findByDataSaidaBetween(dataSaida, datachegada);
//		List<ViagemDTO> list = new ArrayList<>();
//		for (Viagem viagem : viagens) {
//			list.add(new ViagemDTO(viagem));
//		}
//		return list;
//	}

	@Transactional
	public ViagemNewDTO update(ViagemNewDTO viagemDto) {
		Optional<Viagem> optional = viagemRepository.findById(viagemDto.getId());
		if(optional.isPresent()) {
			Viagem viagem = optional.get();
			viagem.setEmbarcacao(viagemDto.getEmbarcacao());
			viagemRepository.save(viagem);
			
			return new ViagemNewDTO();
		} else {
			throw new OjectNotFoundException("viagem não encontrada");
		}
		
		
	}
}
