package com.rest.boat.api.domain.model;

import java.io.Serializable;
import java.util.ArrayList;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name = "CAD_EMPRESA")
public class Empresa implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String nome;
	@Column(name = "nome_fantasia")
	private String nomeFantasia;
	private String cnpj;

	@OneToMany(mappedBy = "empresa")
	private List<Embarcacao> embarcacaos = new ArrayList<>();

	@OneToMany(mappedBy = "empresa")
	private List<TelefoneEmpresa> telefones = new ArrayList<>();

	@OneToMany(mappedBy = "empresa", cascade = CascadeType.ALL)
	private List<EnderecoEmpresa> enderecos = new ArrayList<>();
	@OneToMany(mappedBy = "empresa")
	private List<Departamento> departamentos = new ArrayList<>();

	public Empresa() {
		super();
	}

	public Empresa(Long id, String nome, String nomeFantasia, String cnpj) {
		super();
		this.id = id;
		this.nome = nome;
		this.nomeFantasia = nomeFantasia;
		this.cnpj = cnpj;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNomeFantasia() {
		return nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public List<Embarcacao> getEmbarcacaos() {
		return embarcacaos;
	}

	public void setEmbarcacaos(List<Embarcacao> embarcacaos) {
		this.embarcacaos = embarcacaos;
	}

	public List<TelefoneEmpresa> getTelefones() {
		return telefones;
	}

	public void setTelefones(List<TelefoneEmpresa> telefones) {
		this.telefones = telefones;
	}

	public List<EnderecoEmpresa> getEnderecos() {
		return enderecos;
	}

	public void setEnderecos(List<EnderecoEmpresa> enderecos) {
		this.enderecos = enderecos;
	}

}
