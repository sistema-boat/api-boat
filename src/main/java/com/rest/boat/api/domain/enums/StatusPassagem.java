package com.rest.boat.api.domain.enums;

public enum StatusPassagem {
	CANCELADA, VENDIDA, REMARCADA;
	
	public static StatusPassagem find(String value) {
		for (StatusPassagem passagem : StatusPassagem.values()) {
			if (passagem.name().equalsIgnoreCase(value))
				return passagem;
		}
		throw new IllegalArgumentException("Status Passagem inválido");
		
	}

}
