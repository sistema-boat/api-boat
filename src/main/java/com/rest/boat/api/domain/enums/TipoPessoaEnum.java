package com.rest.boat.api.domain.enums;

public enum TipoPessoaEnum {
	PESSOAFISICA, PESSOAJURIDICA;

	public static TipoPessoaEnum find(String value) {
		for(TipoPessoaEnum pessoa : TipoPessoaEnum.values()) {
			if (pessoa.name().equalsIgnoreCase(value))
				return pessoa;
		}
		throw new IllegalArgumentException("Tipo Pessoa inválido");
	}
	
}
