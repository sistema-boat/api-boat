package com.rest.boat.api.domain.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.rest.boat.api.domain.model.Cliente;
import com.rest.boat.api.domain.model.Viagem;

public class PassagemNewDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long id;
	private Integer poltrona;
	@Temporal(TemporalType.DATE)
	@JsonFormat(pattern = "dd/MM/yyyy")
	private Date dataDaCompra;
	private Cliente cliente;
	private Viagem viagem;
	private String tipoVenda;
	private String statusPassagem;
	public PassagemNewDTO() {
		super();
	}
	public PassagemNewDTO(Long id, Integer poltrona, Date dataDaCompra, Cliente cliente, Viagem viagem,
			String tipoVenda, String statusPassagem) {
		super();
		this.id = id;
		this.poltrona = poltrona;
		this.dataDaCompra = dataDaCompra;
		this.cliente = cliente;
		this.viagem = viagem;
		this.tipoVenda = tipoVenda;
		this.statusPassagem = statusPassagem;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getPoltrona() {
		return poltrona;
	}

	public void setPoltrona(Integer poltrona) {
		this.poltrona = poltrona;
	}

	public Date getDataDaCompra() {
		return dataDaCompra;
	}

	public void setDataDaCompra(Date dataDaCompra) {
		this.dataDaCompra = dataDaCompra;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Viagem getViagem() {
		return viagem;
	}

	public void setViagem(Viagem viagem) {
		this.viagem = viagem;
	}

	public String getTipoVenda() {
		return tipoVenda;
	}

	public void setTipoVenda(String tipoVenda) {
		this.tipoVenda = tipoVenda;
	}

	public String getStatusPassagem() {
		return statusPassagem;
	}

	public void setStatusPassagem(String statusPassagem) {
		this.statusPassagem = statusPassagem;
	}	
}
