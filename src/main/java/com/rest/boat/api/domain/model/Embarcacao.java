package com.rest.boat.api.domain.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name = "CAD_EMBARCACAO")
public class Embarcacao implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String nome;
	private String capacidade;
	@Column(name = "numero_identificacao_marinha")
	private String numIdenMar;
	@ManyToOne
	@JoinColumn(name = "empresa_id")
	private Empresa empresa;
	@OneToMany(mappedBy = "embarcacao")
	private List<Viagem> viagens = new ArrayList<>();

	@ManyToMany(mappedBy = "embarcacoes", cascade = CascadeType.ALL)
	private List<Funcionario> funcionarios;

	public Embarcacao() {
		super();
	}

	public Embarcacao(Long id, String nome, String capacidade, String numIdenMar, Empresa empresa) {
		super();
		this.id = id;
		this.nome = nome;
		this.capacidade = capacidade;
		this.numIdenMar = numIdenMar;
		this.empresa = empresa;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCapacidade() {
		return capacidade;
	}

	public void setCapacidade(String capacidade) {
		this.capacidade = capacidade;
	}

	public String getNumIdenMar() {
		return numIdenMar;
	}

	public void setNumIdenMar(String numIdenMar) {
		this.numIdenMar = numIdenMar;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public List<Viagem> getViagens() {
		return viagens;
	}

	public void setViagens(List<Viagem> viagens) {
		this.viagens = viagens;
	}

	public List<Funcionario> getFuncionarios() {
		return funcionarios;
	}

	public void setFuncionarios(List<Funcionario> funcionarios) {
		this.funcionarios = funcionarios;
	}

}
