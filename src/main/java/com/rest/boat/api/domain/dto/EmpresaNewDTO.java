package com.rest.boat.api.domain.dto;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;
public class EmpresaNewDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long id;
	@NotEmpty(message = "Campo obrigatorio")
	private String nome;
	@NotEmpty(message = "Campo obrigatorio")
	private String nomeFantasia;
	//@NotEmpty
	//@CNPJ
	private String cnpj;

	@NotEmpty(message = "Campo obrigatorio")
	private String cep;
	@NotEmpty(message = "Campo obrigatorio")
	private String logradouro;
	@NotEmpty(message = "Campo obrigatorio")
	private String complemento;
	@NotEmpty(message = "Campo obrigatorio")
	private String bairro;
	@NotEmpty(message = "Campo obrigatorio")
	private String localidade;
	@NotEmpty(message = "Campo obrigatorio")
	private String uf;
	private String ddd;

	@NotEmpty(message = "Campo obrigatorio")
	private String telefone1;
	private String telefone2;
	private String telefone3;

	public EmpresaNewDTO() {
		super();
	}

	public EmpresaNewDTO(String nome, String nomeFantasia, String cnpj, String cep, String logradouro,
			String complemento, String bairro, String localidade, String uf, String ddd, String telefone1,
			String telefone2, String telefone3) {
		super();
		this.nome = nome;
		this.nomeFantasia = nomeFantasia;
		this.cnpj = cnpj;
		this.cep = cep;
		this.logradouro = logradouro;
		this.complemento = complemento;
		this.bairro = bairro;
		this.localidade = localidade;
		this.uf = uf;
		this.ddd = ddd;
		this.telefone1 = telefone1;
		this.telefone2 = telefone2;
		this.telefone3 = telefone3;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNomeFantasia() {
		return nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getLocalidade() {
		return localidade;
	}

	public void setLocalidade(String localidade) {
		this.localidade = localidade;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public String getDdd() {
		return ddd;
	}

	public void setDdd(String ddd) {
		this.ddd = ddd;
	}

	public String getTelefone1() {
		return telefone1;
	}

	public void setTelefone1(String telefone1) {
		this.telefone1 = telefone1;
	}

	public String getTelefone2() {
		return telefone2;
	}

	public void setTelefone2(String telefone2) {
		this.telefone2 = telefone2;
	}

	public String getTelefone3() {
		return telefone3;
	}

	public void setTelefone3(String telefone3) {
		this.telefone3 = telefone3;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	
}
