package com.rest.boat.api.domain.dto;

import java.io.Serializable;

import com.rest.boat.api.domain.model.Empresa;

public class EmbarcacaoNewDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long id;
	private String nome;
	private String capacidade;
	private String numIdenMar;
	private Empresa empresa;

	public EmbarcacaoNewDTO() {
		super();
	}

	
	public EmbarcacaoNewDTO(Long id, String nome, String capacidade, String numIdenMar, Empresa empresa) {
		super();
		this.id = id;
		this.nome = nome;
		this.capacidade = capacidade;
		this.numIdenMar = numIdenMar;
		this.empresa = empresa;
	}


	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCapacidade() {
		return capacidade;
	}

	public void setCapacidade(String capacidade) {
		this.capacidade = capacidade;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public String getNumIdenMar() {
		return numIdenMar;
	}


	public void setNumIdenMar(String numIdenMar) {
		this.numIdenMar = numIdenMar;
	}

	
}
