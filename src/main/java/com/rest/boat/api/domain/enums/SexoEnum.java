package com.rest.boat.api.domain.enums;

public enum SexoEnum {
	MASCULINO, FEMININO;
	public static SexoEnum find(String value) {
		for(SexoEnum sexo : SexoEnum.values()) {
			if(sexo.name().equalsIgnoreCase(value))
				return sexo;
		}
		
		throw new IllegalArgumentException("Sexo inválido");
	}

}
