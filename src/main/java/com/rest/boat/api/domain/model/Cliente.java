package com.rest.boat.api.domain.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.rest.boat.api.domain.enums.FaixaEtariaEnum;
import com.rest.boat.api.domain.enums.SexoEnum;
import com.rest.boat.api.domain.enums.TipoDocumentoEnum;
import com.rest.boat.api.domain.enums.TipoPessoaEnum;

@Entity(name = "CAD_CLIENTE")
public class Cliente implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String nome;
	@Column(name = "nome_mae")
	private String nomeMae;
	@JsonFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	@Column(name = "data_nascimento")
	private Date dataNascimento;
	@Enumerated(EnumType.STRING)
	@Column(name = "sexo")
	private SexoEnum sexo;
	private String rg;
	@Enumerated(EnumType.STRING)
	@Column(name = "tipo_pessoa")
	private TipoPessoaEnum tipoPessoa;
	@Enumerated(EnumType.STRING)
	@Column(name = "tipo_documento")
	private TipoDocumentoEnum tipoDocumento;
	@Column(name = "cpf_ou_cnpj")
	private String cpfOuCnpj;
	@Enumerated(EnumType.STRING)
	@Column(name = "faixa_etaria")
	private FaixaEtariaEnum faixaEtaria;
	private String email;
	@OneToMany(mappedBy = "cliente", cascade = CascadeType.ALL)
	private List<TelefoneCliente> telefones = new ArrayList<>();
	@OneToMany(mappedBy = "cliente", cascade = CascadeType.ALL)
	private List<EnderecoCliente> enderecos = new ArrayList<>();
	@OneToMany(mappedBy = "cliente")
	@JsonIgnore
	private List<Passagem> passagens = new ArrayList<>();

	public Cliente() {
		super();
	}

	public Cliente(Long id, String nome, String nomeMae, Date dataNascimento, String sexo, String rg, String tipoPessoa,
			String tipoDocumento, String cpfOuCnpj, String faixaEtaria, String email) {
		super();
		this.id = id;
		this.nome = nome;
		this.nomeMae = nomeMae;
		this.dataNascimento = dataNascimento;
		this.sexo = SexoEnum.find(sexo);
		this.rg = rg;
		this.tipoPessoa = (tipoPessoa == null) ? null : TipoPessoaEnum.find(tipoPessoa);
		this.tipoDocumento = (tipoDocumento == null) ? null : TipoDocumentoEnum.find(tipoDocumento);
		this.cpfOuCnpj = cpfOuCnpj;
		this.faixaEtaria = FaixaEtariaEnum.find(faixaEtaria);
		this.email = email;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNomeMae() {
		return nomeMae;
	}

	public void setNomeMae(String nomeMae) {
		this.nomeMae = nomeMae;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public SexoEnum getSexo() {
		return sexo;
	}

	public void setSexo(SexoEnum sexo) {
		this.sexo = sexo;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public TipoPessoaEnum getTipoPessoa() {
		return tipoPessoa;
	}

	public void setTipoPessoa(TipoPessoaEnum tipoPessoa) {
		this.tipoPessoa = tipoPessoa;
	}

	public TipoDocumentoEnum getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(TipoDocumentoEnum tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getCpfOuCnpj() {
		return cpfOuCnpj;
	}

	public void setCpfOuCnpj(String cpfOuCnpj) {
		this.cpfOuCnpj = cpfOuCnpj;
	}

	public FaixaEtariaEnum getFaixaEtaria() {
		return faixaEtaria;
	}

	public void setFaixaEtaria(FaixaEtariaEnum faixaEtaria) {
		this.faixaEtaria = faixaEtaria;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<EnderecoCliente> getEnderecos() {
		return enderecos;
	}

	public void setEnderecos(List<EnderecoCliente> string) {
		this.enderecos = string;
	}

	public List<Passagem> getPassagens() {
		return passagens;
	}

	public void setPassagens(List<Passagem> passagens) {
		this.passagens = passagens;
	}

	public void setEnderecoCliente(EnderecoCliente enderecoCliente) {
		this.enderecos.add(enderecoCliente);
	}

	public List<TelefoneCliente> getTelefones() {
		return telefones;
	}

	public void setTelefones(List<TelefoneCliente> telefones) {
		this.telefones = telefones;
	}
}
