package com.rest.boat.api.domain.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.rest.boat.api.domain.enums.StatusPassagem;
import com.rest.boat.api.domain.enums.TipoVendaEnum;

@Entity(name = "CAD_PASSAGEM")
public class Passagem implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name = "numero_poltrona")
	private Integer poltrona;
	@JsonFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dataDaCompra;
	@ManyToOne
	//@JsonIgnore
	@JoinColumn(name = "cliente_id")
	private Cliente cliente;
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "viagem_id")
	private Viagem viagem;
	@Enumerated(EnumType.STRING)
	private TipoVendaEnum tipoVenda;
	@Enumerated(EnumType.STRING)
	private StatusPassagem statusPassagem;

	public Passagem() {
		super();
	}

	public Passagem(Long id, Integer poltrona, Date dataDaCompra, Cliente cliente, Viagem viagem,
			String tipoVenda, String statusPassagem) {
		super();
		this.id = id;
		this.poltrona = poltrona;
		this.dataDaCompra = dataDaCompra;
		this.cliente = cliente;
		this.viagem = viagem;
		this.tipoVenda = TipoVendaEnum.find(tipoVenda);
		this.statusPassagem = StatusPassagem.find(statusPassagem);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getPoltrona() {
		return poltrona;
	}

	public void setPoltrona(Integer poltrona) {
		this.poltrona = poltrona;
	}

	public Date getDataDaCompra() {
		return dataDaCompra;
	}

	public void setDataDaCompra(Date dataDaCompra) {
		this.dataDaCompra = dataDaCompra;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Viagem getViagem() {
		return viagem;
	}

	public void setViagem(Viagem viagem) {
		this.viagem = viagem;
	}

	public TipoVendaEnum getTipoVenda() {
		return tipoVenda;
	}

	public void setTipoVenda(TipoVendaEnum tipoVenda) {
		this.tipoVenda = tipoVenda;
	}

	public StatusPassagem getStatusPassagem() {
		return statusPassagem;
	}

	public void setStatusPassagem(StatusPassagem statusPassagem) {
		this.statusPassagem = statusPassagem;
	}

}
