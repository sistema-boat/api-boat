package com.rest.boat.api.domain.enums;

public enum TipoDocumentoEnum {
RG, CPF, CNPJ;
	public static TipoDocumentoEnum find(String value)  {
		for(TipoDocumentoEnum documento : TipoDocumentoEnum.values() ) {
			if(documento.name().equalsIgnoreCase(value))
				return documento;
		}
		throw new IllegalArgumentException("Tipo Documento Invaálido");
}
}