package com.rest.boat.api.domain.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.rest.boat.api.domain.model.Embarcacao;

public class ViagemNewDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long id;
	private String destino;
	private String origem;
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	private Date dataSaida;
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	private Date dataChegada;
	private Embarcacao embarcacao;

	public ViagemNewDTO() {
		super();
	}

	public ViagemNewDTO(Long id, String destino, String origem, Date dataSaida, Date dataChegada,
			Embarcacao embarcacao) {
		super();
		this.id = id;
		this.destino = destino;
		this.origem = origem;
		this.dataSaida = dataSaida;
		this.dataChegada = dataChegada;
		this.embarcacao = embarcacao;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDestino() {
		return destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	public String getOrigem() {
		return origem;
	}

	public void setOrigem(String origem) {
		this.origem = origem;
	}

	public Date getDataSaida() {
		return dataSaida;
	}

	public void setDataSaida(Date dataSaida) {
		this.dataSaida = dataSaida;
	}

	public Date getDataChegada() {
		return dataChegada;
	}

	public void setDataChegada(Date dataChegada) {
		this.dataChegada = dataChegada;
	}

	public Embarcacao getEmbarcacao() {
		return embarcacao;
	}

	public void setEmbarcacao(Embarcacao embarcacao) {
		this.embarcacao = embarcacao;
	}
}
