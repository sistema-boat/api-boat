package com.rest.boat.api.domain.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.rest.boat.api.domain.model.Embarcacao;
import com.rest.boat.api.domain.model.Viagem;

public class ViagemDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long id;
	private String destino;
	private String origem;
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.DATE)
	private Date dataSaida;
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	private Date dataChegada;
//	private List<Passagem> passagens = new ArrayList<>();
//	private Embarcacao embarcacao;

	public ViagemDTO() {
		super();
	}

	public ViagemDTO(Long id, String destino, String origem, Date dataSaida, Date dataChegada, Embarcacao embarcacao) {
		super();
		this.id = id;
		this.destino = destino;
		this.origem = origem;
		this.dataSaida = dataSaida;
		this.dataChegada = dataChegada;
		// this.embarcacao = embarcacao;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDestino() {
		return destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	public String getOrigem() {
		return origem;
	}

	public void setOrigem(String origem) {
		this.origem = origem;
	}

	public Date getDataSaida() {
		return dataSaida;
	}

	public void setDataSaida(Date dataSaida) {
		this.dataSaida = dataSaida;
	}

	public Date getDataChegada() {
		return dataChegada;
	}

	public void setDataChegada(Date dataChegada) {
		this.dataChegada = dataChegada;
	}

//	public List<Passagem> getPassagens() {
//		return passagens;
//	}
//
//	public void setPassagens(List<Passagem> passagens) {
//		this.passagens = passagens;
//	}
//
//	public Embarcacao getEmbarcacao() {
//		return embarcacao;
//	}
//
//	public void setEmbarcacao(Embarcacao embarcacao) {
//		this.embarcacao = embarcacao;
//	}

	public ViagemDTO(Viagem viagem) {
		this.id = viagem.getId();
		this.destino = viagem.getDestino();
		this.origem = viagem.getOrigem();
		this.dataChegada = viagem.getDataChegada();
		this.dataSaida = viagem.getDataSaida();
		// this.passagens = viagem.getPassagens();
		// this.embarcacao = viagem.getEmbarcacao();
	}

}
