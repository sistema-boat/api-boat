package com.rest.boat.api.domain.dto;

import java.io.Serializable;
import java.util.Date;
import com.rest.boat.api.domain.model.Cliente;

public class ClienteDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long id;
	private String nome;
	private String nomeMae;
	private Date dataNascimento;
	private String sexo;
	private String rg;
	private String faixaEtaria;
	private String email;
//	private List<TelefoneCliente> telefones = new ArrayList<>();
//	private List<EnderecoCliente> enderecos = new ArrayList<>();
//	private List<Passagem> passagens = new ArrayList<>();

	public ClienteDTO() {
		super();
	}

	public ClienteDTO(Long id, String nome, String nomeMae, Date dataNascimento, String sexo, String rg,
			String faixaEtaria, String email) {
		super();
		this.id = id;
		this.nome = nome;
		this.nomeMae = nomeMae;
		this.dataNascimento = dataNascimento;
		this.sexo = sexo;
		this.rg = rg;
		this.faixaEtaria = faixaEtaria;
		this.email = email;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNomeMae() {
		return nomeMae;
	}

	public void setNomeMae(String nomeMae) {
		this.nomeMae = nomeMae;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getFaixaEtaria() {
		return faixaEtaria;
	}

	public void setFaixaEtaria(String faixaEtaria) {
		this.faixaEtaria = faixaEtaria;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

//	public List<TelefoneCliente> getTelefones() {
//		return telefones;
//	}
//
//	public void setTelefones(List<TelefoneCliente> telefones) {
//		this.telefones = telefones;
//	}
//
//	public List<EnderecoCliente> getEnderecos() {
//		return enderecos;
//	}
//
//	public void setEnderecos(List<EnderecoCliente> enderecos) {
//		this.enderecos = enderecos;
//	}
//
//	public List<Passagem> getPassagens() {
//		return passagens;
//	}
//
//	public void setPassagens(List<Passagem> passagens) {
//		this.passagens = passagens;
//	}

	public ClienteDTO(Cliente cliente) {
		this.id = cliente.getId();
		nome = cliente.getNome();
		nomeMae = cliente.getNomeMae();
		dataNascimento = cliente.getDataNascimento();
		sexo = cliente.getSexo().toString();
		rg = cliente.getRg();
		faixaEtaria = cliente.getFaixaEtaria().toString();
		email = cliente.getEmail();
		//enderecos = cliente.getEnderecos();
		//telefones = cliente.getTelefones();
	}
}
