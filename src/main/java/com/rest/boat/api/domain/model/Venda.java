package com.rest.boat.api.domain.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.rest.boat.api.domain.enums.TipoVendaEnum;
@Entity(name="CAD_VENDA")
public class Venda implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name="tipo_venda")
	@Enumerated(EnumType.STRING)
	private TipoVendaEnum tipoVenda;
	
	public Venda() {
		super();
	}
	public Venda(Long id, String tipoVenda) {
		super();
		this.id = id;
		this.tipoVenda = TipoVendaEnum.find(tipoVenda);
	}
	 
}
