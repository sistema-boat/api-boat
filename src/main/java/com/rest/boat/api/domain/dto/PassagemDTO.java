package com.rest.boat.api.domain.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.rest.boat.api.domain.model.Passagem;

public class PassagemDTO implements Serializable{
	private static final long serialVersionUID = 1L;
	private Long id;
	private Integer poltrona;
	@Temporal(TemporalType.DATE)
	@JsonFormat(pattern = "dd/MM/yyyy")
	private Date dataDaCompra;
	private String tipoVenda;
	public PassagemDTO() {
		super();
	}
	public PassagemDTO(Long id, Integer poltrona, Date dataDaCompra, String tipoVenda) {
		super();
		this.id = id;
		this.poltrona = poltrona;
		this.dataDaCompra = dataDaCompra;
		this.tipoVenda = tipoVenda;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Integer getPoltrona() {
		return poltrona;
	}
	public void setPoltrona(Integer poltrona) {
		this.poltrona = poltrona;
	}
	public Date getDataDaCompra() {
		return dataDaCompra;
	}
	public void setDataDaCompra(Date dataDaCompra) {
		this.dataDaCompra = dataDaCompra;
	}
	public String getTipoVenda() {
		return tipoVenda;
	}
	public void setTipoVenda(String tipoVenda) {
		this.tipoVenda = tipoVenda;
	}
	
	public PassagemDTO (Passagem passagem) {
		this.id = passagem.getId();
		this.poltrona = passagem.getPoltrona();
		this.dataDaCompra = passagem.getDataDaCompra();
		tipoVenda = passagem.getTipoVenda().toString();
	}
}
