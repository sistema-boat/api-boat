package com.rest.boat.api.domain.dto;

import java.io.Serializable;


import com.rest.boat.api.domain.enums.SexoEnum;
import com.rest.boat.api.domain.model.Funcionario;

public class FuncionarioDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private String nome;
	private String nomeDaMae;
	private SexoEnum sexo;

	public FuncionarioDTO() {
		super();
	}

	public FuncionarioDTO(String nome, String nomeDaMae, SexoEnum sexo) {
		super();
		this.nome = nome;
		this.nomeDaMae = nomeDaMae;
		this.sexo = sexo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNomeDaMae() {
		return nomeDaMae;
	}

	public void setNomeDaMae(String nomeDaMae) {
		this.nomeDaMae = nomeDaMae;
	}

	public SexoEnum getSexo() {
		return sexo;
	}

	public void setSexo(SexoEnum sexo) {
		this.sexo = sexo;
	}

	public FuncionarioDTO(Funcionario funcionario) {
		this.nome = funcionario.getNome();
		this.nomeDaMae = funcionario.getNomeDaMae();
		this.sexo = funcionario.getSexo();
	}

}
