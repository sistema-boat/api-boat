package com.rest.boat.api.domain.enums;

public enum TipoVendaEnum {

	DINHEIRO, CARTAO;
	
	public static TipoVendaEnum find(String tipoVenda) {
		for (TipoVendaEnum venda : TipoVendaEnum.values()) {
			if (venda.name().equalsIgnoreCase(tipoVenda))
				return venda;
		}
		throw new IllegalArgumentException("Tipo Venda Inválida");
	}
}
