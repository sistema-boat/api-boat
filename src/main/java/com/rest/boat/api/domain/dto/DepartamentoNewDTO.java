package com.rest.boat.api.domain.dto;

import java.io.Serializable;

public class DepartamentoNewDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private String nome;

	public DepartamentoNewDTO() {
		super();
	}

	public DepartamentoNewDTO(String nome) {
		super();
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
}
