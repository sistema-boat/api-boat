package com.rest.boat.api.domain.dto;

import java.io.Serializable;


import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

import com.rest.boat.api.domain.model.Embarcacao;

public class EmbarcacaoDTO implements Serializable{
	private static final long serialVersionUID = 1L;
	private Long id;
	@NotEmpty(message="Preenchimento obrigatório")
	@Length(min = 5, max = 80, message = "O tamanho dever ser entre 5 e 80 caracteres")
	private String nome;
	@NotEmpty(message = "Preenchimento obrigatório")
	@Length(min = 2, max = 5, message = "O tamanho dever ser entre 2 e 5 caracteres")
	private String capacidade;
	public EmbarcacaoDTO() {
		
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCapacidade() {
		return capacidade;
	}
	public void setCapacidade(String capacidade) {
		this.capacidade = capacidade;
	}
	
	public EmbarcacaoDTO ( Embarcacao embarcacao) {
		this.id = embarcacao.getId();
		this.nome = embarcacao.getNome();
		this.capacidade = embarcacao.getCapacidade();
	}
	
}
