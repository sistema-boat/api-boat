package com.rest.boat.api.domain.dto;

import java.io.Serializable;

import com.rest.boat.api.domain.model.Empresa;


public class EmpresaDTO implements Serializable{
	private static final long serialVersionUID = 1L;
	private String nome;
	private String nomeFantasia;
	public EmpresaDTO() {
		super();
	}
	public EmpresaDTO(String nome, String nomeFantasia) {
		super();
		this.nome = nome;
		this.nomeFantasia = nomeFantasia;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getNomeFantasia() {
		return nomeFantasia;
	}
	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}
	
	public EmpresaDTO(Empresa c) {
	this.nome = c.getNome();
	this.nomeFantasia = c.getNomeFantasia();
	}
}
