package com.rest.boat.api.domain.enums;

public enum FaixaEtariaEnum {
ADULTO, IDOSO, CRIANCA;

	public static FaixaEtariaEnum find(String value) {
		for (FaixaEtariaEnum faixa : FaixaEtariaEnum.values()) {
			if(faixa.name().equalsIgnoreCase(value))
				return faixa;
		}
		
		throw new IllegalArgumentException("Faixa Etaria Inválida");
	}
	
}

