package com.rest.boat.api.domain.enums;

public enum EstadoCivilEnum {
	SOLTEIRO,SOLTEIRA,CASADO,CASADA,VIUVA, VIUVO, DIVORCIADA,DIVORCIDADO;
	public static EstadoCivilEnum find(String value) {
		for (EstadoCivilEnum civil : EstadoCivilEnum.values()) {
			if(civil.name().equalsIgnoreCase(value))
				return civil;
		}
		
		throw new IllegalArgumentException("Estado Civil  Inválido");
	}

}
