package com.rest.boat.api.domain.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.rest.boat.api.domain.enums.EstadoCivilEnum;
import com.rest.boat.api.domain.enums.SexoEnum;

@Entity(name = "CAD_FUNCIONARIO")
public class Funcionario implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name = "nome_Funcionario")
	private String nome;
	@Column(name = "nome_da_mae")
	private String nomeDaMae;
	@Column(name = "data_nascimento")
	@Temporal(TemporalType.DATE)
	private Date dataNascimento;
	@Enumerated(EnumType.STRING)
	@Column(name = "sexo")
	private SexoEnum sexo;
	private String cpf;
	private String rg;
	private String email;
	@Enumerated(EnumType.STRING)
	@Column(name = "estado_civil")
	private EstadoCivilEnum estadoCivil;
	@OneToMany(mappedBy = "funcionario", cascade = CascadeType.ALL)
	private List<EnderecoFuncionario> enderecos = new ArrayList<>();
	@OneToMany(mappedBy = "funcionario", cascade = CascadeType.ALL)
	private List<TelefoneFuncionario> telefones =  new ArrayList<>();
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name="CAD_EMBARCACAO_FUNCIONARIO", joinColumns = {@JoinColumn(name="EMBARCACAO_ID")}, inverseJoinColumns = {@JoinColumn(name="FUNCIONARIO_ID")})
	private List<Embarcacao> embarcacoes =  new ArrayList<>();

	@ManyToOne
	@JoinColumn(name = "cargo_id_fk")
	private Cargo cargo;

	public Funcionario() {
		super();
	}

	public Funcionario(Long id, String nome, String nomeDaMae, Date dataNascimento, String sexo, String cpf, String rg,
			String email, String estadoCivil, Embarcacao embarcacao) {
		super();
		this.id = id;
		this.nome = nome;
		this.nomeDaMae = nomeDaMae;
		this.dataNascimento = dataNascimento;
		this.sexo = SexoEnum.find(sexo);
		this.cpf = cpf;
		this.rg = rg;
		this.email = email;
		this.estadoCivil = EstadoCivilEnum.find(estadoCivil);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNomeDaMae() {
		return nomeDaMae;
	}

	public void setNomeDaMae(String nomeDaMae) {
		this.nomeDaMae = nomeDaMae;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public SexoEnum getSexo() {
		return sexo;
	}

	public void setSexo(SexoEnum sexo) {
		this.sexo = sexo;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public EstadoCivilEnum getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(EstadoCivilEnum estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public List<EnderecoFuncionario> getEnderecos() {
		return enderecos;
	}

	public void setEnderecos(List<EnderecoFuncionario> enderecos) {
		this.enderecos = enderecos;
	}

	public List<TelefoneFuncionario> getTelefones() {
		return telefones;
	}

	public void setTelefones(List<TelefoneFuncionario> telefones) {
		this.telefones = telefones;
	}

	public List<Embarcacao> getEmbarcacoes() {
		return embarcacoes;
	}

	public void setEmbarcacoes(List<Embarcacao> embarcacoes) {
		this.embarcacoes = embarcacoes;
	}
	
	
}
