package com.rest.boat.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiBoatRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiBoatRestApplication.class, args);
	}

}
