package com.rest.boat.api.controller;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.rest.boat.api.domain.dto.DepartamentoNewDTO;
import com.rest.boat.api.domain.model.Departamento;
import com.rest.boat.api.manager.DepartamentoManager;

@RestController
@RequestMapping("/departamentos")
public class DepartamentoController {
	
	@Autowired
	private DepartamentoManager departamentoManager;
	
	@PostMapping
	public ResponseEntity<Void> inserir(@RequestBody DepartamentoNewDTO departamentoDto) {
		Departamento obj = departamentoManager.fromDTO(departamentoDto);
		obj = departamentoManager.inserir(obj);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getId())
				.toUri();
		return ResponseEntity.created(uri).build();
	}
}
