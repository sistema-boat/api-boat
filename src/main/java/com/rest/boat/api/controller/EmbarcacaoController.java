package com.rest.boat.api.controller;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.rest.boat.api.domain.dto.EmbarcacaoDTO;
import com.rest.boat.api.domain.dto.EmbarcacaoNewDTO;
import com.rest.boat.api.domain.model.Embarcacao;
import com.rest.boat.api.manager.EmbarcacaoManager;

@RestController
@RequestMapping("/embarcacoes")
public class EmbarcacaoController {
	@Autowired
	private EmbarcacaoManager embarcacaoManager;

	@GetMapping("{id}")
	public ResponseEntity<Embarcacao> find(@PathVariable Long id) {
		Embarcacao embarcacao = embarcacaoManager.find(id);
		return ResponseEntity.ok().body(embarcacao);
	}

	@PostMapping
	public ResponseEntity<?> insert(@Valid @RequestBody EmbarcacaoNewDTO objDto) {
		Embarcacao obj = embarcacaoManager.fromDTO(objDto);
		obj = embarcacaoManager.insert(obj);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getId()).toUri();
		return ResponseEntity.created(uri).build();
	}


	
	@PutMapping("/{id}")
	public ResponseEntity<?> update( @RequestBody EmbarcacaoNewDTO embarcacao, @PathVariable Long id) {
		embarcacao.setId(id);
		embarcacao = embarcacaoManager.update(embarcacao);
		return ResponseEntity.noContent().build();
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		embarcacaoManager.delete(id);
		return ResponseEntity.noContent().build();
	}

	@GetMapping
	public ResponseEntity<List<EmbarcacaoDTO>> findAll() {
		List<Embarcacao> list = embarcacaoManager.findAll();
		List<EmbarcacaoDTO> listDto = list.stream().map(embarcacao -> new EmbarcacaoDTO(embarcacao))
				.collect(Collectors.toList());
		return ResponseEntity.ok().body(listDto);
	}

	@GetMapping("/page")
	public ResponseEntity<Page<EmbarcacaoDTO>> findPage(@RequestParam(value = "page", defaultValue = "0") Integer page,
			@RequestParam(value = "linesPerPage", defaultValue = "24") Integer linesPerPage,@RequestParam(value = "orderBy", defaultValue = "nome") String orderBy,
			@RequestParam(value = "direction", defaultValue = "ASC") String direction) {
		Page<Embarcacao> list = embarcacaoManager.findPage(page, linesPerPage, orderBy, direction);
		Page<EmbarcacaoDTO> listDto = list.map(embarcacao -> new EmbarcacaoDTO(embarcacao));
		return ResponseEntity.ok().body(listDto);

	}

}
