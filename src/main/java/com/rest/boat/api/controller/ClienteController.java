package com.rest.boat.api.controller;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.rest.boat.api.domain.dto.ClienteDTO;
import com.rest.boat.api.domain.dto.ClienteNewDTO;
import com.rest.boat.api.domain.model.Cliente;
import com.rest.boat.api.manager.ClienteManager;

@RestController
@RequestMapping("/clientes")
public class ClienteController {
	@Autowired
	private ClienteManager clienteManager;

	@GetMapping("{id}")
	public ResponseEntity<Cliente> find(@PathVariable Long id) {
		Cliente Cliente = clienteManager.find(id);
		return ResponseEntity.ok().body(Cliente);
	}

	@PostMapping
	public ResponseEntity<Cliente> insert(@Valid @RequestBody ClienteNewDTO clienteNewDto) {
		Cliente obj = clienteManager.fromDTO(clienteNewDto);
		obj = clienteManager.insert(obj);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getId())
				.toUri();
		return ResponseEntity.created(uri).build();
	}
	@PutMapping("/{id}")
	public ResponseEntity<?> update(@Validated @RequestBody ClienteNewDTO cliente, @PathVariable Long id) {
		cliente.setId(id);
		clienteManager.update(cliente);
		return ResponseEntity.noContent().build();
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> delete(@PathVariable Long id) {
		clienteManager.delete(id);
		return ResponseEntity.noContent().build();
	}

	@GetMapping
	public ResponseEntity<List<ClienteDTO>> findAll() {
		return ResponseEntity.ok(clienteManager.listarCleinte());
	}

	@GetMapping("/page")
	public ResponseEntity<Page<ClienteDTO>> findPage(@RequestParam(value = "page", defaultValue = "0") Integer page,
			@RequestParam(value = "linesPerPage", defaultValue = "10") Integer linesPerPage,
			@RequestParam(value = "orderBy", defaultValue = "nome") String orderBy,
			@RequestParam(value = "direction", defaultValue = "ASC") String direction) {
		Page<Cliente> list = clienteManager.findPage(page, linesPerPage, orderBy, direction);
		Page<ClienteDTO> listDto = list.map(cliente -> new ClienteDTO(cliente));
		return ResponseEntity.ok().body(listDto);

	}

}
