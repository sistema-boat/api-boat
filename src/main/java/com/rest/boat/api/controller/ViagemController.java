package com.rest.boat.api.controller;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.rest.boat.api.domain.dto.ViagemDTO;
import com.rest.boat.api.domain.dto.ViagemNewDTO;
import com.rest.boat.api.domain.model.Viagem;
import com.rest.boat.api.manager.ViagemManager;

@RestController
@RequestMapping("/viagens")
public class ViagemController {
	@Autowired
	private ViagemManager viagemManager;

	@PostMapping
	public ResponseEntity<Void> insert(@Valid @RequestBody ViagemNewDTO objDto) {
		Viagem obj = viagemManager.fromDTO(objDto);
		obj = viagemManager.insert(obj);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getId()).toUri();
		return ResponseEntity.created(uri).build();
	}
	
	@GetMapping
	public ResponseEntity<List<ViagemDTO>> listar() {
		return ResponseEntity.ok(viagemManager.listarViagem());
		
	}
	
	@GetMapping("/{origem}/{destino}")
	public ResponseEntity<List<ViagemDTO>> find(@PathVariable ("origem") String origem, @PathVariable ("destino") String destino){
		List<ViagemDTO> viagem = viagemManager.getViagemByOrigem(origem, destino);
		return viagem.isEmpty() ? ResponseEntity.noContent().build() : ResponseEntity.ok(viagem);
}

	@PutMapping("/{id}")
	public ResponseEntity<?> update(@RequestBody ViagemNewDTO viagem, @PathVariable Long id) {
		viagem.setId(id);
		viagemManager.update(viagem);
		return ResponseEntity.noContent().build();
		
	}
	
	
}
