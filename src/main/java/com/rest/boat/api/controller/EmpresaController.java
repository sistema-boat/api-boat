package com.rest.boat.api.controller;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.rest.boat.api.domain.dto.EmpresaDTO;
import com.rest.boat.api.domain.dto.EmpresaNewDTO;
import com.rest.boat.api.domain.model.Empresa;
import com.rest.boat.api.manager.EmpresaManager;

@RestController
@RequestMapping("/empresas")
public class EmpresaController {

	@Autowired
	private EmpresaManager empresaManager;
	
		
	@GetMapping("{id}")
	public ResponseEntity<Empresa> find(@PathVariable Long id) {
		Empresa Empresa = empresaManager.find(id);
		return ResponseEntity.ok().body(Empresa);
	}

	@PostMapping
	public ResponseEntity<Empresa> insert(@Valid @RequestBody EmpresaNewDTO empresaNewDto) {
		Empresa obj = empresaManager.fromDTO(empresaNewDto);
		obj = empresaManager.insert(obj);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getId())
				.toUri();
		return ResponseEntity.created(uri).build();
	}
	@PutMapping("/{id}")
	public ResponseEntity<?> update(@RequestBody EmpresaNewDTO empresa, @PathVariable Long id) {
		empresa.setId(id);
		empresaManager.update(empresa);
		return ResponseEntity.noContent().build();
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> delete(@PathVariable Long id) {
		empresaManager.delete(id);
		return ResponseEntity.noContent().build();
	}

	@GetMapping
	public ResponseEntity<List<EmpresaDTO>> findAll() {
		return ResponseEntity.ok(empresaManager.listarCleinte());
	}

	
}
