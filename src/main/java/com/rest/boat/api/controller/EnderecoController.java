package com.rest.boat.api.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.rest.boat.api.domain.dtoIntegracaoViacep.EnderecoDTOIntegracaoViaCep;

@RestController
@RequestMapping("/enderecos")
public class EnderecoController {
	@GetMapping("/{cep}")
	public ResponseEntity<EnderecoDTOIntegracaoViaCep> ObterCep(@PathVariable("cep") String cep) {
		RestTemplate resTemplete = new RestTemplate();
		String uri = "http://viacep.com.br/ws/{cep}/json/";
		Map<String, String> params = new HashMap<String, String>();
		params.put("cep", cep);
		EnderecoDTOIntegracaoViaCep enderecoDTOIntegracaoViaCep = resTemplete.getForObject(uri,
				EnderecoDTOIntegracaoViaCep.class, params);
		return new ResponseEntity<EnderecoDTOIntegracaoViaCep>(enderecoDTOIntegracaoViaCep, HttpStatus.OK);
	}

	
	
}
