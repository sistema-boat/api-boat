package com.rest.boat.api.controller;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.rest.boat.api.domain.dto.FuncionarioDTO;
import com.rest.boat.api.domain.dto.FuncionarioNewDTO;
import com.rest.boat.api.domain.model.Funcionario;
import com.rest.boat.api.manager.FuncionarioManager;

@RestController
@RequestMapping("/funcionarios")
public class FuncionarioController {

	@Autowired
	private FuncionarioManager funcionarioManager;
	
	@GetMapping("{id}")
	public ResponseEntity<Funcionario> find(@PathVariable Long id) {
		Funcionario Funcionario = funcionarioManager.find(id);
		return ResponseEntity.ok().body(Funcionario);
	}

	@PostMapping
	public ResponseEntity<Funcionario> insert(@Valid @RequestBody FuncionarioNewDTO  funcionarioNewDto) {
		Funcionario obj = funcionarioManager.fromDTO(funcionarioNewDto);
		obj = funcionarioManager.insert(obj);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getId())
				.toUri();
		return ResponseEntity.created(uri).build();
	}
	@PutMapping("/{id}")
	public ResponseEntity<?> update(@Validated @RequestBody FuncionarioNewDTO funcionario, @PathVariable Long id) {
		funcionario.setId(id);
		funcionarioManager.update(funcionario);
		return ResponseEntity.noContent().build();
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> delete(@PathVariable Long id) {
		funcionarioManager.delete(id);
		return ResponseEntity.noContent().build();
	}

	@GetMapping
	public ResponseEntity<List<FuncionarioDTO>> findAll() {
		return ResponseEntity.ok(funcionarioManager.listarFuncionario());
	}

//	@GetMapping("/page")
//	public ResponseEntity<Page<FuncionarioDTO>> findPage(@RequestParam(value = "page", defaultValue = "0") Integer page,
//			@RequestParam(value = "linesPerPage", defaultValue = "10") Integer linesPerPage,
//			@RequestParam(value = "orderBy", defaultValue = "nome") String orderBy,
//			@RequestParam(value = "direction", defaultValue = "ASC") String direction) {
//		Page<Funcionario> list = FuncionarioManager.findPage(page, linesPerPage, orderBy, direction);
//		Page<FuncionarioDTO> listDto = list.map(Funcionario -> new FuncionarioDTO(Funcionario));
//		return ResponseEntity.ok().body(listDto);
//
//	}
	
}
