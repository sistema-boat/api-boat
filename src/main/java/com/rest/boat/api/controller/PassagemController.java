package com.rest.boat.api.controller;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import com.rest.boat.api.domain.dto.PassagemDTO;
import com.rest.boat.api.domain.dto.PassagemNewDTO;
import com.rest.boat.api.domain.model.Passagem;

import com.rest.boat.api.manager.PassagemManager;

@RestController
@RequestMapping("/passagens")
public class PassagemController {

	@Autowired
	private PassagemManager passagemManager;

	@GetMapping("/{id}")
	public ResponseEntity<Passagem> find(@PathVariable Long id) {
		Passagem passagem = passagemManager.find(id);
		return ResponseEntity.ok(passagem);
	}

	@GetMapping
	public ResponseEntity<List<PassagemDTO>> listarPassagens() {
		return ResponseEntity.ok(passagemManager.listarPassagens());
		
	}
	
	@PostMapping
	public ResponseEntity<Void> insert(@Valid @RequestBody PassagemNewDTO objNewDto) {
		Passagem obj = passagemManager.fromDTO(objNewDto);
		obj = passagemManager.insert(obj);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getId()).toUri();
		return ResponseEntity.created(uri).build();

	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> delete(@PathVariable Long id) {
		passagemManager.delete(id);
		return ResponseEntity.noContent().build();
	}

	@PutMapping("/{id}")
	public ResponseEntity<?> update(@Validated @RequestBody PassagemNewDTO passagem, @PathVariable Long id) {
		passagem.setId(id);
		passagemManager.update(passagem);
		return ResponseEntity.noContent().build();
	}
}
