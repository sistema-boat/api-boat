package com.rest.boat.api.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.rest.boat.api.domain.model.EnderecoCliente;
@Repository
public interface EnderecoRepository extends JpaRepository<EnderecoCliente, Long>{
	EnderecoCliente findByClienteId(Long id);
	
}
