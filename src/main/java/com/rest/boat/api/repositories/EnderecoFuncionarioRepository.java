package com.rest.boat.api.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.rest.boat.api.domain.model.EnderecoFuncionario;
@Repository
public interface EnderecoFuncionarioRepository extends JpaRepository<EnderecoFuncionario, Long>{
	EnderecoFuncionario findByFuncionarioId(Long id);

}
