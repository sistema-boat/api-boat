package com.rest.boat.api.repositories;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.rest.boat.api.domain.model.Viagem;

@Repository
public interface ViagemRepository extends JpaRepository<Viagem, Long>{
	
	@Query("SELECT u, function('date_format', u.dataSaida, '%d,%m,%y') as dataSaida from CAD_VIAGEM u where origem = ?1 and destino = ?2")
	List<Viagem> findByOrigemAndDestino(String origem, String destino);
	
	//List<Viagem> findByDataSaidaBetween(Date dataSaida, Date datachegada);
}
