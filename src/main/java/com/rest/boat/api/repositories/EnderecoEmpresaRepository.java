package com.rest.boat.api.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import com.rest.boat.api.domain.model.EnderecoEmpresa;
@Repository
public interface EnderecoEmpresaRepository extends JpaRepository<EnderecoEmpresa, Long>{

	EnderecoEmpresa findByEmpresaId(Long id);

}
