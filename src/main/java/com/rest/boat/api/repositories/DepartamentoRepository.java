package com.rest.boat.api.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.rest.boat.api.domain.model.Departamento;
@Repository
public interface DepartamentoRepository extends JpaRepository<Departamento, Long>{

}
