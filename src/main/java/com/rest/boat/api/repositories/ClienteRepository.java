package com.rest.boat.api.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.rest.boat.api.domain.model.Cliente;
@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Long>{
	//@Query(value ="SELECT u, date_format(dataNascimento, '%d, %m, %y') as dataNascimento from CAD_CLIENTE u")
	@Query(value="SELECT c FROM CAD_CLIENTE c")
	List<Cliente> findAlld1();
	Cliente findById(Cliente cliente);
}
