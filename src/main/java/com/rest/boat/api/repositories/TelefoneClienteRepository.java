package com.rest.boat.api.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.rest.boat.api.domain.model.TelefoneCliente;
@Repository
public interface TelefoneClienteRepository extends JpaRepository<TelefoneCliente, Long>{
	TelefoneCliente findByClienteId(Long id);
}
