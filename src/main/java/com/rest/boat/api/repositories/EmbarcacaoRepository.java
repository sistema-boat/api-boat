package com.rest.boat.api.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.rest.boat.api.domain.model.Embarcacao;
@Repository
public interface EmbarcacaoRepository extends JpaRepository<Embarcacao, Long>{
	Embarcacao findByEmpresaId(Long id);
	
}
