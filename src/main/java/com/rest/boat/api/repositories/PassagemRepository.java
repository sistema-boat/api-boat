package com.rest.boat.api.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.rest.boat.api.domain.model.Passagem;

@Repository
public interface PassagemRepository extends JpaRepository<Passagem, Long>{
	
}
