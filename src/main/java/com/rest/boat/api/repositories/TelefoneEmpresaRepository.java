package com.rest.boat.api.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.rest.boat.api.domain.model.TelefoneEmpresa;

@Repository
public interface TelefoneEmpresaRepository extends JpaRepository<TelefoneEmpresa, Long>{
	
	TelefoneEmpresa findByEmpresaId(Long id);

}
